package com.example.assignment_10

import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.example.assignment_10.databinding.LayoutImageBinding

class ImageSlidePager :
    RecyclerView.Adapter<ImageSlidePager.ImageSlidePagerViewHolder>() {
    private val images = mutableListOf<Image>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ImageSlidePagerViewHolder(
            LayoutImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ImageSlidePager.ImageSlidePagerViewHolder,
        position: Int
    ) =
        holder.onBind()
    

    inner class ImageSlidePagerViewHolder(private val binding: LayoutImageBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var model: Image

        fun onBind() {
            model = images[adapterPosition]
            binding.ivImage.setImageResource(model.image)
            binding.tvTitle.text = model.title
            binding.tvContent.text = model.content
            binding.tvPageInfo.text = "${adapterPosition + 1} / $itemCount"
        }
    }

    override fun getItemCount(): Int {
        return images.size
    }

    fun setData(images: MutableList<Image>) {
        this.images.clear()
        this.images.addAll(images)
        notifyDataSetChanged()
    }

}