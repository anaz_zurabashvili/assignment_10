package com.example.assignment_10

typealias MIPMAP = R.mipmap
typealias STRINGS = R.string

data class Image(var image:Int, var title:String, var content: String)