package com.example.assignment_10

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.assignment_10.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var imageSlidePager: ImageSlidePager
    private val images = mutableListOf<Image>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        setData()
        imageSlidePager = ImageSlidePager()
        imageSlidePager.setData(images)
        binding.vp2Images.adapter = imageSlidePager
        binding.vp2Images.orientation = ViewPager2.ORIENTATION_VERTICAL
    }

    private fun setData() {
        images.add(
            Image(
                MIPMAP.ic_2_foreground,
                getString(STRINGS.title_1),
                getString(STRINGS.content_1)
            )
        )
        images.add(
            Image(
                MIPMAP.ic_2_foreground,
                getString(STRINGS.title_2),
                getString(STRINGS.content_2)
            )
        )
        images.add(
            Image(
                MIPMAP.ic_2_foreground,
                getString(STRINGS.title_3),
                getString(STRINGS.content_3)
            )
        )
        images.add(
            Image(
                MIPMAP.ic_2_foreground,
                getString(STRINGS.title_4),
                getString(STRINGS.content_4)
            )
        )
        images.add(
            Image(
                MIPMAP.ic_2_foreground,
                getString(STRINGS.app_name),
                getString(STRINGS.content_5)
            )
        )
        images.add(
            Image(
                MIPMAP.ic_2_foreground,
                getString(STRINGS.title_6),
                getString(STRINGS.content_6)
            )
        )
    }
}